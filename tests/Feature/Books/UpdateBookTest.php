<?php

namespace Tests\Feature\Books;

use App\Models\Book;
use Tests\TestCase;

/**
 * @mixin Book
 */
class UpdateBookTest extends TestCase
{
    public function test_update_a_book()
    {
        $changes = [
            'name'  => 'new name',
            'price' => 123,
            'free'  => true,
            'image' => Book::factory()->make()->image
        ];

        $this->patchJson(route('books.update', Book::first()), $changes)->assertOk();

        $this->assertDatabaseHas('books', $changes);
    }
}
