<?php

namespace Tests\Feature\Books;

use App\Models\Book;
use Tests\TestCase;

class DestroyBookTest extends TestCase
{
    public function test_delete_a_book()
    {
        $book = Book::inRandomOrder()->first()->id;

        $this->assertDatabaseHas('books', ['id' => $book]);

        $this->deleteJson(route('books.destroy', $book))->assertOk();

        $this->assertDatabaseMissing('books', ['id' => $book]);
    }
}
