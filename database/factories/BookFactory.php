<?php

namespace Database\Factories;

use App\Models\Book;
use App\Models\Comment;
use App\Models\Like;
use App\Models\View;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @mixin Book
 */
class BookFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'name'       => $this->faker->name,
            'price'      => $this->faker->numberBetween(10, 100),
            'free'       => $this->faker->boolean,
            'image'      => $this->faker->image,
        ];
    }
}
