<?php

namespace App\Models\Relationships;

use App\Models\Book;
use App\Models\User;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

trait LikeRelationships
{
    /**
     * Relationship between book and likes
     *
     * @return BelongsTo
     */
    public function book(): BelongsTo
    {
        return $this->belongsTo(Book::class);
    }

    /**
     * Relationship between user and like
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
