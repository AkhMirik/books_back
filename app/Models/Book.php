<?php

namespace App\Models;

use App\Models\Relationships\BookRelationships;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use HasFactory, BookRelationships;

    protected static $unguarded = true;


}
