<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Relationships\LikeRelationships;

class Like extends Model
{
    use HasFactory, LikeRelationships;

    protected static $unguarded = true;
}
