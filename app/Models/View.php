<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Relationships\ViewRelationships;

class View extends Model
{
    use HasFactory, ViewRelationships;

    protected static $unguarded = true;

}
